CREATE SEQUENCE "bg_comments_seq";

CREATE TABLE "bg_comments" (  
  "coid" INT NOT NULL DEFAULT nextval('bg_comments_seq'),
  "cid" INT NULL DEFAULT '0',
  "created" INT NULL DEFAULT '0',
  "author" VARCHAR(200) NULL DEFAULT NULL,
  "authorId" INT NULL DEFAULT '0',
  "ownerId" INT NULL DEFAULT '0',
  "mail" VARCHAR(200) NULL DEFAULT NULL,
  "url" VARCHAR(200) NULL DEFAULT NULL,
  "ip" VARCHAR(64) NULL DEFAULT NULL,
  "agent" VARCHAR(200) NULL DEFAULT NULL,
  "text" TEXT NULL DEFAULT NULL,
  "type" VARCHAR(16) NULL DEFAULT 'comment',
  "status" VARCHAR(16) NULL DEFAULT 'approved',
  "parent" INT NULL DEFAULT '0',
  PRIMARY KEY ("coid")
);

CREATE INDEX "bg_comments_cid" ON "bg_comments" ("cid");
CREATE INDEX "bg_comments_created" ON "bg_comments" ("created");


--
-- Table structure for table "bg_contents"
--

CREATE SEQUENCE "bg_contents_seq";

CREATE TABLE "bg_contents" (  
 "cid" INT NOT NULL DEFAULT nextval('bg_contents_seq'),
  "title" VARCHAR(200) NULL DEFAULT NULL,
  "slug" VARCHAR(200) NULL DEFAULT NULL,
  "created" INT NULL DEFAULT '0',
  "modified" INT NULL DEFAULT '0',
  "text" TEXT NULL DEFAULT NULL,
  "order" INT NULL DEFAULT '0',
  "author_id" INT NULL DEFAULT '0',
  "template" VARCHAR(32) NULL DEFAULT NULL,
  "type" VARCHAR(16) NULL DEFAULT 'post',
  "status" VARCHAR(16) NULL DEFAULT 'publish',
  "password" VARCHAR(32) NULL DEFAULT NULL,
  "commentsnum" INT NULL DEFAULT '0',
  "allowcomment" CHAR(1) NULL DEFAULT '0',
  "allowping" CHAR(1) NULL DEFAULT '0',
  "allowfeed" CHAR(1) NULL DEFAULT '0',
  "parent" INT NULL DEFAULT '0',
  PRIMARY KEY ("cid"),
  UNIQUE ("slug")
);

CREATE INDEX "bg_contents_created" ON "bg_contents" ("created");

--
-- Table structure for table "bg_fields"
--

CREATE TABLE "bg_fields" (
  "cid" INT NOT NULL,
  "name" VARCHAR(200) NOT NULL,
  "type" VARCHAR(8) NULL DEFAULT 'str',
  "str_value" TEXT NULL DEFAULT NULL,
  "int_value" INT NULL DEFAULT '0',
  "float_value" REAL NULL DEFAULT '0',
  PRIMARY KEY  ("cid","name")
);

CREATE INDEX "bg_fields_int_value" ON "bg_fields" ("int_value");
CREATE INDEX "bg_fields_float_value" ON "bg_fields" ("float_value");

--
-- Table structure for table "bg_metas"
--

CREATE SEQUENCE "bg_metas_seq";

CREATE TABLE "bg_metas" (  
  "mid" INT NOT NULL DEFAULT nextval('bg_metas_seq'),
  "name" VARCHAR(200) NULL DEFAULT NULL,
  "slug" VARCHAR(200) NULL DEFAULT NULL,
  "type" VARCHAR(16) NOT NULL DEFAULT '',
  "description" VARCHAR(200) NULL DEFAULT NULL,
  "count" INT NULL DEFAULT '0',
  "order" INT NULL DEFAULT '0',
  "parent" INT NULL DEFAULT '0',
  PRIMARY KEY ("mid")
);

CREATE INDEX "bg_metas_slug" ON "bg_metas" ("slug");


--
-- Table structure for table "bg_options"
--

CREATE TABLE "bg_options" (  
  "name" VARCHAR(32) NOT NULL DEFAULT '',
  "user" INT NOT NULL DEFAULT '0',
  "value" TEXT NULL DEFAULT NULL,
  PRIMARY KEY ("name","user")
);

--
-- Table structure for table "bg_relationships"
--

CREATE TABLE "bg_relationships" (  
  "cid" INT NOT NULL DEFAULT '0',
  "mid" INT NOT NULL DEFAULT '0',
  PRIMARY KEY ("cid","mid")
); 

