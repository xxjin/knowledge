<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../taglibs.jsp"%>
<!DOCTYPE html>
<html>
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title>设备管理系统--首页</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="${ctx}/resources/assets/global/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${ctx}/resources/assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${ctx}/resources/assets/global/plugins/bootstrap/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${ctx}/resources/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"
	rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="${ctx}/resources/assets/global/css/components.min.css"
	rel="stylesheet" id="style_components" type="text/css" />
<link href="${ctx}/resources/assets/global/css/plugins.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="${ctx}/resources/assets/layouts/layout/css/layout.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${ctx}/resources/assets/layouts/layout/css/themes/blue.min.css"
	rel="stylesheet" type="text/css" id="style_color" />
<link href="${ctx}/resources/assets/layouts/layout/css/custom.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link href="${ctx}/resources/artDialog5.0/skins/default.css"
	rel="stylesheet" />

<style>
.page-content {
	padding: 0 !important;
	background-color: #f5f5f5;
}
</style>
<script type="text/javascript">
	var ctx = "${ctx}";
	/**
	function init_iframe() {
	    if ($("#_left_menu").offset().top != $("#_right_menu").offset().top) {
	        $(".my_top_module").remove();
	        $("#select_top_module").show();
	    }
	}**/

	//修改密码
	function changePasswd() {
		art.dialog({
			title : '修改密码',
			content : document.getElementById("changePwd"),
			fixed : true,
			width : 400,
			height : 100,
			okValue : '确定',
			cancelValue : '取消',
			ok : function() {//异步请求
				//密码校验
				var newp = $("#newpwd").val();
				var rep = $("#repwd").val();
				var pwd = $("#passwd").val();
				var flag = false;

				if (newp == '' || rep == '' || pwd == '') {
					alert("密码不能为空!");
					flag = true;
					return false;
				}
				if (newp.length < 6) {
					alert("密码长度最少为6位");
					flag = true;
					return false;
				}
				if (!(newp == rep)) {
					alert("两次密码不一致!");
					flag = true;
					return false;
				}

				if (!flag) {
					$.ajax({
						url : '${ctx}/user/changePasswd',
						cache : false,
						data : {
							'passwd' : pwd,
							'newpwd' : newp
						},
						type : 'post',
						success : function(tag) {
							if (eval(tag)) {
								alert("密码重置成功!");
							} else {
								alert("密码重置失败!");
							}
						}
					});
				}

			},
			cancel : function() {
			}
		});

	}
</script>
</head>
<!-- END HEAD -->

<body scroll="no" style="overflow: hidden"
	class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">

	<form action="" id="changePwd" style="display: none;">
		<table>
			<tr>
				<td><label>原密码:</label></td>
				<td colspan="2"><input type="password" name="passwd"
					id="passwd" style="width: 160px; margin-left: 10px;" /></td>
			</tr>
			<tr>
				<td><label>新密码:</label></td>
				<td><input type="password" name="newpwd" id="newpwd"
					style="width: 160px; margin-left: 10px;" /></td>
				<td><label id="newp">* 新密码至少为6位</label></td>
			</tr>

			<tr>
				<td><label>确认密码:</label></td>
				<td><input type="password" name="repwd" id="repwd"
					style="width: 160px; margin-left: 10px;" /></td>
				<td><label id="rep"></label></td>

			</tr>

		</table>
	</form>
	<!-- BEGIN HEADER -->
	<div class="page-header navbar navbar-fixed-top">
		<!-- BEGIN HEADER INNER -->
		<div class="page-header-inner ">
			<!-- BEGIN LOGO -->
			<div class="page-logo">
				<a href="#"> <img
					src="${ctx}/resources/img/logo-blog2.png"
					alt="logo" class="logo-default" />
				</a>
				<div class="menu-toggler sidebar-toggler">
					<span> </span>
				</div>
			</div>
			<!-- END LOGO -->

			<!-- BEGIN RESPONSIVE MENU TOGGLER -->
			<a href="javascript:;" class="menu-toggler responsive-toggler"
				data-toggle="collapse" data-target=".navbar-collapse"> <span></span>
			</a>
			<!-- END RESPONSIVE MENU TOGGLER -->
			<!-- BEGIN TOP NAVIGATION MENU -->
			<div id="_left_menu"
				class="hor-menu  hor-menu-light hidden-sm hidden-xs">
				<ul class="nav navbar-nav">
					<li class="mega-menu-dropdown "><a class="dropdown-toggle"
						data-hover="megamenu-dropdown" data-close-others="true"
						href="javascript:_MAP('7', '1', 'admin.php?c=home&amp;m=main');"
						data-original-title="" title=""> <i class="icon-home"> </i>首页
					</a></li>
					<li class="mega-menu-dropdown active"><a class="dropdown-toggle"
						data-hover="megamenu-dropdown" data-close-others="true"
						href="javascript:_MAP('7', '1', 'admin.php?c=home&amp;m=main');"
						data-original-title="" title=""> <i class="icon-home"> </i>系统管理
					</a></li>
				</ul>
			</div>
			<div id="_right_menu" class="top-menu">
				<ul class="nav navbar-nav pull-right">
					<!-- BEGIN USER LOGIN DROPDOWN -->
					<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
					<li class="dropdown dropdown-user"><a href="javascript:;"
						class="dropdown-toggle" data-toggle="dropdown"
						data-hover="dropdown" data-close-others="true"> <img alt=""
							class="img-circle"
							src="${ctx }/resources/assets/layouts/layout/img/avatar3.jpg" />
							<span class="username username-hide-on-mobile">
								${SESSION_USER.userName } </span> <i class="fa fa-angle-down"></i>
					</a>
						<ul class="dropdown-menu dropdown-menu-default">

							<li><a href="javascript:changePasswd()"> <i
									class="icon-rocket"></i> 修改密码
							</a></li>
							<li><a href="${ctx }/auth/logout"><i class="icon-key"></i>
									注销 </a></li>
						</ul></li>
					<!-- END USER LOGIN DROPDOWN -->
					<!-- BEGIN QUICK SIDEBAR TOGGLER -->
					<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
					<li class="dropdown dropdown-quick-sidebar-toggler"><a
						href="javascript:logout();void(0);" class="dropdown-toggle"> <font></font><i
							class="icon-logout"></i>
					</a></li>
					<!-- END QUICK SIDEBAR TOGGLER -->
				</ul>
			</div>
			<!-- END TOP NAVIGATION MENU -->
		</div>
		<!-- END HEADER INNER -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN HEADER & CONTENT DIVIDER -->
	<div class="clearfix"></div>
	<!-- END HEADER & CONTENT DIVIDER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar-wrapper">
			<!-- BEGIN SIDEBAR -->
			<div class="page-sidebar navbar-collapse collapse">
				<!-- BEGIN SIDEBAR MENU -->
				<ul class="page-sidebar-menu  page-header-fixed "
					data-keep-expanded="false" data-auto-scroll="true"
					data-slide-speed="200" style="padding-top: 20px">
					<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
					<li class="sidebar-toggler-wrapper hide">
						<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
						<div class="sidebar-toggler">
							<span></span>
						</div> <!-- END SIDEBAR TOGGLER BUTTON -->
					</li>

					<li class="nav-item start active open"><a href="javascript:;"
						class="nav-link nav-toggle"> <i class="icon-home"></i> <span
							class="title"> 系统管理</span> <span class="arrow open"></span>
					</a>
						<ul class="sub-menu">
							<li class="nav-item start active open"><a
								href="javascript:right('${ctx }/system/user/list');"
								class="nav-link "> <span class="title">用户管理</span>
							</a></li>
							<li class="nav-item  "><a
								href="javascript:right('${ctx }/config/network')"
								class="nav-link "> <span class="title">角色管理</span>
							</a></li>
							 <li class="nav-item  "><a
								href="javascript:right('${ctx }/admin/article/list')"
								class="nav-link "> <span class="title">文章管理</span>
							</a></li>

						</ul></li>
				</ul>
				<!-- END SIDEBAR MENU -->
			</div>
			<!-- END SIDEBAR -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<!-- BEGIN CONTENT BODY -->
			<div class="page-content">
				<iframe name="right" id="rightMain" src="" frameborder="false"
					scrolling="auto"
					style="border: none; margin-bottom: 0px; height: 70px;"
					width="100%" height="auto" allowtransparency="true"></iframe>
			</div>

			<!-- END CONTENT BODY -->
		</div>
		<!-- END CONTENT -->

	</div>
	<!-- END CONTAINER -->

	<!--[if lt IE 9]>
<script src="${ctx}/resources/assets/global/plugins/respond.min.js"></script>
<script src="${ctx}/resources/assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
	<!-- BEGIN CORE PLUGINS -->
	<script src="${ctx}/resources/assets/global/plugins/jquery.min.js"
		type="text/javascript"></script>
	<script
		src="${ctx}/resources/assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="${ctx}/resources/assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="${ctx}/resources/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"
		type="text/javascript"></script>
	<script
		src="${ctx}/resources/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script
		src="${ctx}/resources/assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="${ctx}/resources/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="${ctx}/resources/assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<script
		src="${ctx}/resources/assets/pages/scripts/table-datatables-managed.min.js"
		type="text/javascript"></script>
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script
		src="${ctx}/resources/assets/layouts/layout/scripts/layout.min.js"
		type="text/javascript"></script>
	<script
		src="${ctx}/resources/assets/layouts/layout/scripts/demo.min.js"
		type="text/javascript"></script>
	<script
		src="${ctx}/resources/assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script src="${ctx}/resources/artDialog5.0/artDialog.min.js"
		type="text/javascript"></script>
	<script type="text/javascript">
		var getWindowSize = function() {
			return [ "Height", "Width" ].map(function(name) {
				return window["inner" + name]
						|| document.compatMode === "CSS1Compat"
						&& document.documentElement["client" + name]
						|| document.body["client" + name]
			});
		}
		window.onload = function() {
			if (!+"\v1" && !document.querySelector) { // for IE6 IE7
				document.body.onresize = resize;
			} else {
				window.onresize = resize;
			}
			function resize() {
				wSize();
				return false;
			}
		}
		function wSize() {
			var str = getWindowSize();
			var strs = new Array(); //定义一数组
			strs = str.toString().split(","); //字符分割
			var heights = strs[0] - 80, Body = $('body');
			$('#rightMain').height(heights);
		}
		function logout() {
			if (confirm("您确定要退出吗？"))
				top.location = '${ctx}/auth/logout';
			return false;
		}
		function right(url) {
			$("#rightMain").attr('src', url);
		}
		$(function() {
			//init_iframe();
			wSize();

			$(".nav-link").click(
					function() {
						that = $(this);
						that.parent().siblings().removeClass("active")
								.removeClass("open").end().addClass("active")
								.addClass("open");
					})

		});
	</script>

	<!-- END THEME LAYOUT SCRIPTS -->
</body>
</html>