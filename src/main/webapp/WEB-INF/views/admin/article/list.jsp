<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>文章管理</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="${ctx}/resources/assets/global/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${ctx}/resources/assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${ctx}/resources/assets/global/plugins/bootstrap/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${ctx}/resources/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"
	rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="${ctx}/resources/assets/global/css/components.min.css"
	rel="stylesheet" id="style_components" type="text/css" />
<link href="${ctx}/resources/assets/global/css/plugins.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="${ctx}/resources/assets/layouts/layout/css/layout.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${ctx}/resources/assets/layouts/layout/css/themes/blue.min.css"
	rel="stylesheet" type="text/css" id="style_color" />

<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
<link href="${ctx}/resources/artDialog5.0/skins/default.css"
	rel="stylesheet" />
<link href="${ctx}/resources/assets/layouts/layout/css/custom.min.css"
	rel="stylesheet" type="text/css" />
<style>
.page-content {
	margin-left: 0 !important;
	background-color: #f5f5f5 !important;
}

#pages {
	padding: 0;
	text-align: right
}

#pages a, #pages span {
	display: inline-block;
	height: 28px;
	line-height: 28px;
	background: #fff;
	border: 1px solid #e3e3e3;
	text-align: center;
	color: #333;
	padding: 0 15px;
	margin-left: 1px;
}

#pages a.a1 {
	background: url(../images/pages.png) no-repeat 0 5px;
	padding: 0
}

#pages a:hover {
	background: #f1f1f1;
	color: #000;
	text-decoration: none;
}

#pages span {
	background: #368ee0;
	color: #fff;
}
</style>
<script type="text/javascript">
	var ctx = "${ctx}";
	
	function page(i){
		$("#page").val(i);
		$("#myform").submit();
	}
</script>
</head>
<body class="page-content-white page-footer-fixed" index="nheader"
	style="background-color: #f5f5f5;">


	<!-- BEGIN CONTAINER -->
	<div class="page-container" style="margin-bottom: 0px !important;">
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<!-- BEGIN CONTENT BODY -->
			<div class="page-content">
				<div class="page-bar">
					<ul class="page-breadcrumb mylink">
						<li><i class="fa fa-users"></i> 文章管理</li>
						<li> <a href="${ctx }/admin/article/edit"><i class="fa fa-plus"></i> 添加</a> </li>
					</ul>


				</div>

				<div class="mytopsearch">
					<form method="post" action="" name="searchform" id="searchform">
						<label>分类：</label> <label> <select class="form-control"
							name="groupId">
								<option value="">--</option>
								<option value="1">待审核会员</option>
								<option value="2">OAuth会员</option>
								<option value="3">普通会员</option>
								<option value="4">商业会员</option>
						</select>
						</label> 发布状态：<label> <select class="form-control" name="deptId">
								<option value="">全部</option>
								<option value="0">未发布</option>
								<option value="1">已发布</option>
						</select>
						</label>标题： <label><input type="text" class="form-control"
							value="" placeholder="" name="title" /></label>  <label>
							<button type="submit" class="btn green btn-sm">
								<i class="fa fa-search"></i> 搜索
							</button>
						</label>
					</form>
				</div>
				<div class="portlet light bordered">
					<div class="portlet-body">
						<div class="table-scrollable v3table">
							<form action="" method="post" name="myform" id="myform">
								<input type="hidden" id="page" name="p" value="${p }">
								<table class="table">
									<thead>
										<tr>
											<th width="10"></th>
											<th>id</th>
											<th class="sorting" name="username" width="300">标题</th>
											<th class="sorting" name="groupid">分类</th>
											<th class="sorting" name="experience">发布时间</th>
											<th class="sorting" name="experience">作者</th>
											<th class="sorting" name="score">状态</th>
											<th class="sorting" name="money">评论数</th>
											<th class="dr_option">操作</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${page.list }" var="bgArticle" varStatus="vs">
											<tr id="dr_row_${bgArticle.cid }">
												<td><input name="ids[]" type="checkbox"
													class="dr_select toggle md-check" value="${bgArticle.cid  }" /></td>
												<td>${bgArticle.cid }</td>
												<td><a onclick="dr_dialog_member('${bgArticle.cid  }')"
													href="javascript:;"> ${bgArticle.title } </a></td>
												<td> </td>
												<td><fmt:formatDate value="${ bgArticle.created}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
												<td>${bgArticle.authorName  }</td>
												<td>
												
												<c:if test="${bgArticle.type eq 'post_draft'}">
														草稿
													</c:if> 
													<c:if test="${bgArticle.type eq 'post'}">
														<font color="red">${bgArticle.status }</font>
													</c:if></td>
												<td>${bgArticle.commentsnum  }</td>
											 
												<td class="dr_option"><a class="aedit"
													href="${ctx }/admin/article/edit/${bgArticle.cid }"> <i
														class="fa fa-edit"></i> 编辑
												</a><a class="alist"
													href="${ctx }/space/${bgArticle.cid }"> <i
														class="fa fa-database"></i> 删除
												</a></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</form>
							<div id="pages">
								<a>共${page.totalRow}条</a>
								<c:if test="${page.pageNumber != 1 }">
									<a href="javascript:page(1)">首页</a>
								</c:if>
								<c:if test="${page.pageNumber != 1 }">
									<a href="javascript:page(${page.pageNumber-1})">上一页</a>
								</c:if>

								<c:if test="${page.totalPage<= 6 }">
									<c:set var="begin" value="1"></c:set>
									<c:set var="end" value="${page.totalPage }"></c:set>

								</c:if>

								<c:if test="${page.totalPage>6 }">
									<c:set var="begin" value="${page.pageNumber-2 }"></c:set>
									<c:set var="end" value="${page.pageNumber+3 }"></c:set>
									<c:if test="${begin <1}">
										<c:set var="begin" value="1"></c:set>
										<c:set var="end" value="6"></c:set>
									</c:if>
									<c:if test="${end >page.totalPage}">
										<c:set var="begin" value="${page.totalPage -5}"></c:set>
										<c:set var="end" value="${page.totalPage }"></c:set>
									</c:if>
								</c:if>


								<c:forEach begin="${begin }" end="${end }" var="i">
									<c:if test="${page.pageNumber eq i }">
										<span>${i }</span>
									</c:if>
									<c:if test="${page.pageNumber != i }">
										<a href="javascript:page(${i })">${i} </a>
									</c:if>

								</c:forEach>
								<c:if test="${page.pageNumber < page.totalPage }">
									<a href="javascript:page(${page.pageNumber+1})">下一页</a>
								</c:if>
								<c:if test="${page.pageNumber != page.totalPage }">
									<a href="javascript:page(${page.totalPage})">最后一页
									</a>
								</c:if>
							</div>
						</div>
					</div>
				</div>

			</div>

			<!-- END CONTENT BODY -->
		</div>
		<!-- END CONTENT -->

	</div>
	<!-- END CONTAINER -->

	<!--[if lt IE 9]>
<script src="${ctx}/resources/assets/global/plugins/respond.min.js"></script>
<script src="${ctx}/resources/assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
	<!-- BEGIN CORE PLUGINS -->
	<script src="${ctx}/resources/assets/global/plugins/jquery.min.js"
		type="text/javascript"></script>
	<script
		src="${ctx}/resources/assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="${ctx}/resources/assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="${ctx}/resources/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"
		type="text/javascript"></script>
	<script
		src="${ctx}/resources/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script
		src="${ctx}/resources/assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="${ctx}/resources/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="${ctx}/resources/assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<script
		src="${ctx}/resources/assets/pages/scripts/table-datatables-managed.min.js"
		type="text/javascript"></script>
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script
		src="${ctx}/resources/assets/layouts/layout/scripts/layout.min.js"
		type="text/javascript"></script>
	<script
		src="${ctx}/resources/assets/layouts/layout/scripts/demo.min.js"
		type="text/javascript"></script>
	<script
		src="${ctx}/resources/assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script src="${ctx}/resources/artDialog5.0/artDialog.min.js"
		type="text/javascript"></script>
	<script type="text/javascript">
		$(function() {
			$("#pages a").first().addClass("noloading");
		});
	</script>
	<!-- END THEME LAYOUT SCRIPTS -->
</body>
</html>