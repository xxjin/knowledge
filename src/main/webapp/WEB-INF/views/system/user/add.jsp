<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>添加用户</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="${ctx}/resources/assets/global/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${ctx}/resources/assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${ctx}/resources/assets/global/plugins/bootstrap/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${ctx}/resources/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"
	rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="${ctx}/resources/assets/global/css/components.min.css"
	rel="stylesheet" id="style_components" type="text/css" />
<link href="${ctx}/resources/assets/global/css/plugins.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="${ctx}/resources/assets/layouts/layout/css/layout.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${ctx}/resources/assets/layouts/layout/css/themes/blue.min.css"
	rel="stylesheet" type="text/css" id="style_color" />

<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
<link href="${ctx}/resources/artDialog5.0/skins/default.css"
	rel="stylesheet" />
<link href="${ctx}/resources/assets/layouts/layout/css/custom.min.css"
	rel="stylesheet" type="text/css" />
<style>
.page-content {
	margin-left: 0 !important;
	background-color: #f5f5f5 !important;
}

#pages {
	padding: 0;
	text-align: right
}

#pages a, #pages span {
	display: inline-block;
	height: 28px;
	line-height: 28px;
	background: #fff;
	border: 1px solid #e3e3e3;
	text-align: center;
	color: #333;
	padding: 0 15px;
	margin-left: 1px;
}

#pages a.a1 {
	background: url(../images/pages.png) no-repeat 0 5px;
	padding: 0
}

#pages a:hover {
	background: #f1f1f1;
	color: #000;
	text-decoration: none;
}

#pages span {
	background: #368ee0;
	color: #fff;
}
</style>
<script type="text/javascript">
	var ctx = "${ctx}";
</script>
</head>
<body class="page-content-white page-footer-fixed" index="nheader"
	style="background-color: #f5f5f5;">


	<!-- BEGIN CONTAINER -->
	<div class="page-container" style="margin-bottom: 0px !important;">
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<!-- BEGIN CONTENT BODY -->
			<div class="page-content">
				<div class="page-bar">
					<ul class="page-breadcrumb mylink">
						<li  ><i class="fa fa-users"></i> <a href="${ctx }/system/user/list" class="blue">用户管理</a><i class="fa fa-circle"></i></li>
						<li>添加用户</li>
					</ul>


				</div>
				<div class="mytopsearch">
					<form method="post" class="form-horizontal" action="${ctx }/system/user/add"
						name="addform" id="addform">
						<div class="form-body">
						<h3 class="block">添加新用户 </h3>
						<div class="form-group">
								<label class="col-md-3 control-label">登陆名：</label> <label
									class="col-md-4"><input type="text"
									class="form-control" value="" placeholder="" name="userName" /></label>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label"> 姓名： </label><label
									class="col-md-4"><input type="text"
									class="form-control" value="" name="realName" /></label>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">会员组：</label> <label
									class="col-md-4"> <select class="form-control"
									name="groupId">
										<option value="">--</option>
										<option value="1">待审核会员</option>
										<option value="2">OAuth会员</option>
										<option value="3">普通会员</option>
										<option value="4">商业会员</option>
								</select>
								</label>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">所属部门：</label><label
									class="col-md-4"> <select class="form-control"
									name="deptId">
										<option value="">全部</option>
								</select>
								</label>
							</div>
							
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<button type="submit" class="btn btn-circle green">提交</button>
									<a  href="${ctx }/system/user/list"
										class="btn btn-circle grey-salsa btn-outline">取消</a>
								</div>
							</div>
						</div>

					</form>
				</div>


			</div>

			<!-- END CONTENT BODY -->
		</div>
		<!-- END CONTENT -->

	</div>
	<!-- END CONTAINER -->

	<!--[if lt IE 9]>
<script src="${ctx}/resources/assets/global/plugins/respond.min.js"></script>
<script src="${ctx}/resources/assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
	<!-- BEGIN CORE PLUGINS -->
	<script src="${ctx}/resources/assets/global/plugins/jquery.min.js"
		type="text/javascript"></script>
	<script
		src="${ctx}/resources/assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="${ctx}/resources/assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="${ctx}/resources/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"
		type="text/javascript"></script>
	<script
		src="${ctx}/resources/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script
		src="${ctx}/resources/assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="${ctx}/resources/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="${ctx}/resources/assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<script
		src="${ctx}/resources/assets/pages/scripts/table-datatables-managed.min.js"
		type="text/javascript"></script>
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script
		src="${ctx}/resources/assets/layouts/layout/scripts/layout.min.js"
		type="text/javascript"></script>
	<script
		src="${ctx}/resources/assets/layouts/layout/scripts/demo.min.js"
		type="text/javascript"></script>
	<script
		src="${ctx}/resources/assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script src="${ctx}/resources/artDialog5.0/artDialog.min.js"
		type="text/javascript"></script>
	<script type="text/javascript">
		$(function() {

		});
	</script>
	<!-- END THEME LAYOUT SCRIPTS -->
</body>
</html>