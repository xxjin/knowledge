<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="taglibs.jsp"%>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="zh-cn">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8" />
<title>红枫技术网</title>
<meta name="keywords"
	content="java在线考试，java学习>
    <meta name="description" content="" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="www.feng.red" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="${ctx}/resources/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="${ctx}/resources/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="${ctx}/resources/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/assets/pages/css/search.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="${ctx}/resources/assets/layouts/layout3/css/layout.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/resources/assets/layouts/layout3/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="${ctx}/resources/assets/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css" />
     <link href="${ctx}/resources/css/lib.css" rel="stylesheet" type="text/css" />
    
    <!-- END THEME LAYOUT STYLES -->
    <!--[if lt IE 9]>
    <script src="${ctx}/resources/assets/global/plugins/respond.min.js"></script>
    <script src="${ctx}/resources/assets/global/plugins/excanvas.min.js"></script>
    <![endif]-->
    <!-- BEGIN CORE PLUGINS -->
    <script src="${ctx}/resources/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="${ctx}/resources/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="${ctx}/resources/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="${ctx}/resources/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
    <script src="${ctx}/resources/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="${ctx}/resources/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="${ctx}/resources/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="${ctx}/resources/assets/global/scripts/app.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="${ctx}/resources/assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
    <script src="${ctx}/resources/assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
    <script src="${ctx}/resources/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
    <!-- END THEME LAYOUT SCRIPTS -->

    <!--关键JS开始-->
    <script type="text/javascript">var memberpath = "/";
</script>
<!--关键js结束-->
<!-- END HEAD -->
</head>
<body class="page-container-bg-solid page-header-menu-fixed page-boxed">
	<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<div class="page-header-top">

			<div class="container">
				<div class="row" id="dr_mytop">
					<script type="text/javascript">
						$
								.ajax({
									type : "GET",
									url : "http://vip.finecms.net/index.php?c=api&m=html&name=mytop.html&params=&"
											+ Math.random(),
									dataType : "jsonp",
									success : function(data) {
										$("#dr_mytop").html(data.html);
									}
								});
					</script>
					<!-- 这一段内容采用动态调用的方式 -->
				</div>
				<!-- BEGIN LOGO -->
				<div class="page-logo">
					<a href="http://vip.finecms.net/"> <img
						src="${ctx}/resources/assets/layouts/layout3/img/logo.png"
						alt="红枫技术网" class="logo-default">
					</a>
				</div>
				<!-- END LOGO -->
				<!-- BEGIN 菜单按钮 -->
				<a href="javascript:;" class="menu-toggler"></a>
				<!-- END 菜单按钮 -->
				<!-- BEGIN 会员登录信息 -->
				<div class="top-menu" id="dr_member_info"></div>
				<script type="text/javascript">
					$
							.ajax({
								type : "GET",
								async : false,
								url : "http://vip.finecms.net/index.php?c=api&m=member&format=jsonp",
								dataType : "jsonp",
								success : function(json) {
									$("#dr_member_info").html(json.html);
								},
								error : function() {
								}
							});
				</script>
				<!-- END 会员登录信息 -->
			</div>
		</div>
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<div class="page-header-menu">
			<div class="container2">
				<!-- 全站搜索框 -->
				<form class="search-form" method="get" target="_blank"
					action="http://vip.finecms.net/index.php">
					<input name="c" type="hidden" value="so"> <input
						name="module" type="hidden" value="">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="输入搜索关键字"
							name="keyword"> <span class="input-group-btn"> <a
							href="javascript:;" class="btn submit"> <i
								class="icon-magnifier"></i>
						</a>
						</span>
					</div>
				</form>
				<!-- 调用网站导航菜单 -->
				<div class="hor-menu  ">
					<ul class="nav navbar-nav">
						<li id="dr_nav_0"
							class="menu-dropdown classic-menu-dropdown active"><a
							href="${ctx }" title="FineCMS高级版演示程序">首页</a></li>
						<!--第一层：调用type=0的网站导航数据-->


						<li id="dr_nav_3" class="menu-dropdown classic-menu-dropdown ">
							<a href="http://vip.finecms.net/index.php?s=video" title=""
							target="_blank">视频</a>
						</li>


					</ul>
				</div>
				<!-- END MEGA MENU -->
			</div>
		</div>
		<!-- END HEADER MENU -->
	</div>
	<!-- END HEADER -->
	<div class="page-container">
		<div class="page-content-wrapper">
			<div class="page-head">
				<div class="container">
					<div class="page-title">
						<h1>
							<small>欢迎使用</small>
						</h1>
					</div>
				</div>
			</div>
			<div class="page-content">
				<div class="container">
					<div class="page-content-inner">

						<div class="row">
							<div class="col-md-4">
								<div class="bg-white">
									<link href="${ctx}/resources/assets/pages/slide/slide.css"
										type="text/css" rel="stylesheet">
									<div id="myslide"
										style="width: auto; height: auto; overflow: hidden">
										<table width="100%" cellSpacing="0" cellPadding="0">
											<tr>
												<td class="pic" id="bimg">
													<div class="dis" name="f">
														<a href="http://www.dayrui.com/cms/"
															title="FineCMS海豚版正式发布" target="_blank"><img
															alt="FineCMS海豚版正式发布" style="width: 100%; height: 260px;"
															src="http://www.dayrui.com/assets/p/v21.jpg" border="0"></a>
													</div>
													<div class="undis" name="f">
														<a href="http://www.dayrui.com/cms/" title="强大而灵活的自定义字段功能"><img
															alt="强大而灵活的自定义字段功能" style="width: 100%; height: 260px;"
															src="http://www.dayrui.com/assets/p/v23.jpg" border="0"></a>
													</div>
													<table id="font_hd" width="100%" cellSpacing="0"
														cellPadding="0">
														<tr>
															<td class="title" id="info">
																<div class="dis" name="f">
																	<a href="http://www.dayrui.com/cms/"
																		title="FineCMS海豚版正式发布" target="_blank">FineCMS海豚版正式发布</a>
																</div>
																<div class="undis" name="f">
																	<a href="http://www.dayrui.com/cms/"
																		title="强大而灵活的自定义字段功能">强大而灵活的自定义字段功能</a>
																</div>
															</td>
															<td id="simg" nowrap="nowrap" style="text-align: right">
																<div class="" onclick=play(x[0],0) name="f">1</div>
																<div class="f1" onclick=play(x[1],1) name="f">2</div>
															</td>
														</tr>
													</table> <script src="${ctx}/resources/assets/pages/slide/slide.js"></script>
												</td>
											</tr>
										</table>
									</div>

								</div>
							</div>
							<div class="col-md-8">
								<div class="portlet light  ">
									<div class="portlet-title ">
										<div class="row">
											<div class="col-md-12">
												<!--调用type=2的头条数据-->
												<h4>
													<a cl href="http://www.dayrui.com/shipin/" target="_blank"
														class="title">java在线考试</a>
												</h4>
												<p class="links">
													<a href="http://www.dayrui.com/shipin/" target="_blank">[javase]</a>
													<a href="http://www.dayrui.com/shipin/" target="_blank">[html+javascript]</a>
													<a href="http://www.dayrui.com/shipin/" target="_blank">[数据库]</a>
													<a href="http://www.dayrui.com/bijiao/" target="_blank">[javaee+框架]</a>
												</p>
											</div>
										</div>
									</div>
									<div class="portlet-body ">
										<div class="row">
											<div class="col-md-6">
												<ul class="list-unstyled">
													<!--调用新闻模块的“首页中间”属性的最新10条-->
													<li style="line-height: 23px"><span
														class="badge badge-empty badge-success"></span>&nbsp;<a
														href="/index.php?s=news&c=show&id=167" class="title">服务器防火墙的选择</a></li>
													<li style="line-height: 23px"><span
														class="badge badge-empty badge-success"></span>&nbsp;<a
														href="/index.php?s=news&c=show&id=168" class="title">关注网站常见后门方法大总结...</a></li>
													<li style="line-height: 23px"><span
														class="badge badge-empty badge-success"></span>&nbsp;<a
														href="/index.php?s=news&c=show&id=169" class="title">维基百科推出Nearby页面
															根据...</a></li>
													<li style="line-height: 23px"><span
														class="badge badge-empty badge-success"></span>&nbsp;<a
														href="/index.php?s=news&c=show&id=170" class="title">一夜间仇人变情人：淘宝和百度...</a></li>
													<li style="line-height: 23px"><span
														class="badge badge-empty badge-success"></span>&nbsp;<a
														href="/index.php?s=news&c=show&id=171" class="title">创世与腾讯合作细节曝光：网络...</a></li>
												</ul>
											</div>
											<div class="col-md-6">
												<ul class="list-unstyled">
													<!--调用新闻模块的“首页中间”属性的最新10条-->
													<li style="line-height: 23px"><span
														class="badge badge-empty badge-success"></span>&nbsp;<a
														href="/index.php?s=news&c=show&id=172" class="title">网友爆料两数字.com域名的不完...</a></li>
													<li style="line-height: 23px"><span
														class="badge badge-empty badge-success"></span>&nbsp;<a
														href="/index.php?s=news&c=show&id=173" class="title">阿里签约十家快递公司加速完善...</a></li>
													<li style="line-height: 23px"><span
														class="badge badge-empty badge-success"></span>&nbsp;<a
														href="/index.php?s=news&c=show&id=174" class="title">MSN中国业务欲与Windows整合：...</a></li>
													<li style="line-height: 23px"><span
														class="badge badge-empty badge-success"></span>&nbsp;<a
														href="/index.php?s=news&c=show&id=176" class="title">360路由器：神一般主宰你所有...</a></li>
													<li style="line-height: 23px"><span
														class="badge badge-empty badge-success"></span>&nbsp;<a
														href="/index.php?s=news&c=show&id=178" class="title">微软携手斯皮尔伯格打造Halo真...</a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>


						<!-- 调用新闻模块 -->
						<div class="row">
							 <ul class="list01 clearfix">
			
				<li>
				<div class="spantop bk0">
					<div class="imgtop">
						<img src="http://img.knowledge.csdn.net/upload/base/1471395509081_81.jpg" width="320" height="94" alt="">
					</div>
					<span>
						<a href="http://lib.csdn.net/base/go" target="_blank"><img src="http://img.knowledge.csdn.net/upload/base/1471395508721_721.jpg" width="80" height="80" alt=""></a>
					</span>
				</div>
				<p>
					<a href="http://lib.csdn.net/base/go" class="title" target="_blank">Go</a><span class="content">Go是Google开发的一种编译型，可并行化，并具有垃圾回收功能的编程语言，并且它还是开源的。使用Go编译的程序可以媲美C或C++代码的速度，而且更加安全、支持并行进程。</span>
				</p>
				</li>
			
				<li>
				<div class="spantop bk0">
					<div class="imgtop">
						<img src="http://img.knowledge.csdn.net/upload/base/1452672714363_363.jpg" width="320" height="94" alt="">
					</div>
					<span>
						<a href="http://lib.csdn.net/base/machinelearning" target="_blank"><img src="http://img.knowledge.csdn.net/upload/base/1452498104247_247.jpg" width="80" height="80" alt=""></a>
					</span>
				</div>
				<p>
					<a href="http://lib.csdn.net/base/machinelearning" class="title" target="_blank">机器学习</a><span class="content">机器学习是研究计算机怎样模拟或实现人类的学习行为，以获取新的知识或技能，重新组织已有的知识结构使之不断改善自身的性能。它是人工智能的核心，是使计算机具有智能的根本途径。</span>
				</p>
				</li>
			
				<li>
				<div class="spantop bk0">
					<div class="imgtop">
						<img src="http://img.knowledge.csdn.net/upload/base/1470302448986_986.jpg" width="320" height="94" alt="">
					</div>
					<span>
						<a href="http://lib.csdn.net/base/unity3d" target="_blank"><img src="http://img.knowledge.csdn.net/upload/base/1470302448213_213.jpg" width="80" height="80" alt=""></a>
					</span>
				</div>
				<p>
					<a href="http://lib.csdn.net/base/unity3d" class="title" target="_blank">Unity3D</a><span class="content">Unity3D是一个可以让玩家轻松创建诸如三维视频游戏、建筑可视化、实时三维动画等类型互动内容的多平台的综合型游戏开发工具，是一个全面整合的专业游戏引擎。</span>
				</p>
				</li>
			
				<li>
				<div class="spantop bk0">
					<div class="imgtop">
						<img src="http://img.knowledge.csdn.net/upload/base/1452500466656_656.jpg" width="320" height="94" alt="">
					</div>
					<span>
						<a href="http://lib.csdn.net/base/docker" target="_blank"><img src="http://img.knowledge.csdn.net/upload/base/1452500466484_484.jpg" width="80" height="80" alt=""></a>
					</span>
				</div>
				<p>
					<a href="http://lib.csdn.net/base/docker" class="title" target="_blank">Docker</a><span class="content">Docker是一个开源、可以将任何应用包装在"LXC容器”中运行的工具。如果说VMware、KVM包装的虚拟机，那该工具包装的则是应用。它是一个实至名归的PaaS。</span>
				</p>
				</li>
			
				<li>
				<div class="spantop bk0">
					<div class="imgtop">
						<img src="http://img.knowledge.csdn.net/upload/base/1453368118941_941.jpg" width="320" height="94" alt="">
					</div>
					<span>
						<a href="http://lib.csdn.net/base/spark" target="_blank"><img src="http://img.knowledge.csdn.net/upload/base/1453368118762_762.jpg" width="80" height="80" alt=""></a>
					</span>
				</div>
				<p>
					<a href="http://lib.csdn.net/base/spark" class="title" target="_blank">Apache Spark</a><span class="content">Spark是当前最流行的开源大数据内存计算框架，用Scala语言实现，由UC伯克利大学AMPLab实验室开发并于2010年开源。以通用、易用为目标，高速发展后成为最活跃的Apache开源项目。</span>
				</p>
				</li>
			
				<li>
				<div class="spantop bk0">
					<div class="imgtop">
						<img src="http://img.knowledge.csdn.net/upload/base/1452496102061_61.jpg" width="320" height="94" alt="">
					</div>
					<span>
						<a href="http://lib.csdn.net/base/swift" target="_blank"><img src="http://img.knowledge.csdn.net/upload/base/1452496101906_906.jpg" width="80" height="80" alt=""></a>
					</span>
				</div>
				<p>
					<a href="http://lib.csdn.net/base/swift" class="title" target="_blank">Swift</a><span class="content">Swift是一门新的编程语言，用于编写iOS和OS X应用程序。它结合了C和Objective-C的优点并且不受C兼容性的限制。它使用安全的编程模式并添加了很多新特性，使编程更简单，扩展性更强更有趣。</span>
				</p>
				</li>
			
				<li>
				<div class="spantop bk0">
					<div class="imgtop">
						<img src="http://img.knowledge.csdn.net/upload/base/1470876331774_774.jpg" width="320" height="94" alt="">
					</div>
					<span>
						<a href="http://lib.csdn.net/base/dotnet" target="_blank"><img src="http://img.knowledge.csdn.net/upload/base/1470876331285_285.jpg" width="80" height="80" alt=""></a>
					</span>
				</div>
				<p>
					<a href="http://lib.csdn.net/base/dotnet" class="title" target="_blank">.NET</a><span class="content">.NET Framework是微软用于Windows的新托管代码编程模型，是一个 Microsoft Windows 组件，用户可通过各种分发渠道获得它。</span>
				</p>
				</li>
			
				<li>
				<div class="spantop bk0">
					<div class="imgtop">
						<img src="http://img.knowledge.csdn.net/upload/base/1469610324168_168.jpg" width="320" height="94" alt="">
					</div>
					<span>
						<a href="http://lib.csdn.net/base/oracle" target="_blank"><img src="http://img.knowledge.csdn.net/upload/base/1469610323861_861.jpg" width="80" height="80" alt=""></a>
					</span>
				</div>
				<p>
					<a href="http://lib.csdn.net/base/oracle" class="title" target="_blank">Oracle</a><span class="content">Oracle Database简称Oracle，是甲骨文公司的一款关系型数据库管理系统。具有系统可移植性好、使用方便、功能强等特点，适用于各类大、中、小、微机环境。</span>
				</p>
				</li>
			
				<li>
				<div class="spantop bk0">
					<div class="imgtop">
						<img src="http://img.knowledge.csdn.net/upload/base/1468390230399_399.jpg" width="320" height="94" alt="">
					</div>
					<span>
						<a href="http://lib.csdn.net/base/linux" target="_blank"><img src="http://img.knowledge.csdn.net/upload/base/1468390230134_134.jpg" width="80" height="80" alt=""></a>
					</span>
				</div>
				<p>
					<a href="http://lib.csdn.net/base/linux" class="title" target="_blank">Linux</a><span class="content">Linux是一个免费的，基于POSIX和Unix的多用户、多任务、支持多线程和多CPU的操作系统。它支持32位和64位硬件，且继承了Unix以网络为核心的设计思想，是一个性能稳定的多用户网络操作系统。</span>
				</p>
				</li>
			
		</ul>
						</div>





						<!-- 友情链接 -->
						<div class="row">
							<div class="col-lg-12">
								<div class="portlet light">
									<div class="portlet-title">
										<div class="caption">
											<i class=" icon-layers font-green"></i> <span
												class="caption-subject bold uppercase">友情链接</span>
										</div>
									</div>
									<div class="portlet-body">
										<div class="row">
											<div class="col-md-12">
												<a href="http://www.feng.red" title="" target="_blank">java考试</a>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>



	<!-- BEGIN FOOTER -->
	<!-- BEGIN PRE-FOOTER -->
	<div class="page-prefooter">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6 col-xs-12 footer-block">
					<h2>红枫技术网</h2>
					<p>java开发技术分享，在线学习和在线考试。</p>
				</div>
				<div class="col-md-3 col-sm-6 col-xs12 footer-block">
					<h2>全站搜索</h2>
					<div class="subscribe-form">
						<form class="search-form" method="get" target="_blank"
							action="http://vip.finecms.net/index.php">
							<input name="c" type="hidden" value="so"> <input
								name="module" type="hidden" value="MOD_DIR">
							<div class="input-group">
								<input type="text" class="form-control" placeholder="输入搜索关键字"
									name="keyword"> <span class="input-group-btn">
									<button class="btn" type="submit">搜索</button>
								</span>
							</div>
						</form>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12 footer-block"></div>
				<div class="col-md-3 col-sm-6 col-xs-12 footer-block">
					<h2>联系我们</h2>
					<address class="margin-bottom-40">
						电话: <br> 邮箱: <a href="#">###</a>
					</address>
				</div>
			</div>
		</div>
	</div>
	<!-- END PRE-FOOTER -->
	<!-- BEGIN INNER FOOTER -->
	<div class="page-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-3">@feng.red 2016</div>
				<div class="col-md-9 text-right"></div>
			</div>

		</div>
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
	<!-- END INNER FOOTER -->
	<!-- END FOOTER -->
</body>

</html>
