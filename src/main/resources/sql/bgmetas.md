getAllCategory
===
*查询所有的分类
select #use("cols")# from blog.bg_metas where type=#type# order by sort

getTagsByCid
===
*根据cid 查tag
SELECT	A.* FROM blog.bg_metas A, blog.bg_relationships b WHERE	A .mid = b.mid and a."type"='tag' AND b.cid=#_root# 

getCategoryByCid
===
*根据cid 查分类
SELECT	A.* FROM blog.bg_metas A, blog.bg_relationships b WHERE	A .mid = b.mid and a."type"='category' AND b.cid=#_root# 


deleteTagByCid
===
delete from blog.bg_metas where mid in(SELECT
	A.mid
FROM
	blog.bg_metas A,
	blog.bg_relationships b
WHERE
	A .mid = b.mid
and a."type"='tag'
AND b.cid = #_root#)

sample
===
* 注释

	select #use("cols")# from blog.bg_metas where #use("condition")#

cols
===

	mid,name,slug,type,description,count,sort,parent

updateSample
===

	mid=#mid#,name=#name#,slug=#slug#,type=#type#,description=#description#,count=#count#,sort=#sort#,parent=#parent#

condition
===

	1 = 1  
	@if(!isEmpty(name)){
	 and name=#name#
	@}
	@if(!isEmpty(slug)){
	 and slug=#slug#
	@}
	@if(!isEmpty(type)){
	 and type=#type#
	@}
	@if(!isEmpty(description)){
	 and description=#description#
	@}
	@if(!isEmpty(count)){
	 and count=#count#
	@}
	@if(!isEmpty(sort)){
	 and sort=#sort#
	@}
	@if(!isEmpty(parent)){
	 and parent=#parent#
	@}
	