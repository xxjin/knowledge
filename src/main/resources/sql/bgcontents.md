sample
===
* 注释

	select #use("cols")# from blog.bg_contents where #use("condition")#

cols
===

	cid,title,slug,created,modified,text,sort,(select t.user_name from "public".sys_user t where t.user_id=b.author_id) author_id,template,type,status,password,commentsnum,allowcomment,allowping,allowfeed,parent

updateSample
===

	cid=#cid#,title=#title#,slug=#slug#,created=#created#,modified=#modified#,text=#text#,order=#order#,authorId=#authorid#,template=#template#,type=#type#,status=#status#,password=#password#,commentsNum=#commentsnum#,allowComment=#allowcomment#,allowPing=#allowping#,allowFeed=#allowfeed#,parent=#parent#

condition
===

	1 = 1  
	@if(!isEmpty(title)){
	 and title=#title#
	@}
	@if(!isEmpty(slug)){
	 and slug=#slug#
	@}
	@if(!isEmpty(created)){
	 and created=#created#
	@}
	@if(!isEmpty(modified)){
	 and modified=#modified#
	@}
	@if(!isEmpty(text)){
	 and text=#text#
	@}
	@if(!isEmpty(sort)){
	 and sort=#sort#
	@}
	@if(!isEmpty(authorId)){
	 and author_id=#authorId#
	@}
	@if(!isEmpty(template)){
	 and template=#template#
	@}
	@if(!isEmpty(type)){
	 and type=#type#
	@}
	@if(!isEmpty(status)){
	 and status=#status#
	@}
	@if(!isEmpty(password)){
	 and password=#password#
	@}
	@if(!isEmpty(commentsnum)){
	 and commentsnum=#commentsnum#
	@}
	@if(!isEmpty(allowcomment)){
	 and allowcomment=#allowcomment#
	@}
	@if(!isEmpty(allowping)){
	 and allowping=#allowping#
	@}
	@if(!isEmpty(allowfeed)){
	 and allowfeed=#allowfeed#
	@}
	@if(!isEmpty(parent)){
	 and parent=#parent#
	@}
	
	
	

findById
===
select * from blog.bg_contents where cid=#_root#

pageQuery
===
*分页查询
select 
#page()# 
from blog.v_contents where 
#use("condition")#
	