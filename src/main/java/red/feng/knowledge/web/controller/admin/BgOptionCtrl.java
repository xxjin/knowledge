package red.feng.knowledge.web.controller.admin;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import red.feng.knowledge.model.BgOptions;
import red.feng.knowledge.service.BgOptionsService;
import red.feng.system.model.SysUser;
import red.feng.system.utils.ShiroUtil;

@Controller
@RequestMapping("/admin/blog/options")
public class BgOptionCtrl {
	@Autowired
	private BgOptionsService bgOptionsService;
	@RequestMapping("/editorSize")
	@ResponseBody
	public int editorSize(String size){
		SysUser user=ShiroUtil.getCurrentUser();
		bgOptionsService.updateOption(BgOptions.KEY_EDITOR_SIZE, user.getUserId(), size);
		return 1;
	}
}
