package red.feng.knowledge.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * 前台
 * @author jinxx
 *
 */
@Controller
public class Index {
	Logger log=LoggerFactory.getLogger(Index.class);
	@RequestMapping("/")
	public String index() {
		return "index";
	}
	
}
