package red.feng.knowledge.web.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * 后台管理功能
 * @author jinxx
 *
 */
@Controller
public class AdminCtrl {
	@RequestMapping("/admin/index")
	public String index(){
		return "/admin/index";
	}
}
