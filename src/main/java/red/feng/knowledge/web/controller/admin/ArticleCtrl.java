package red.feng.knowledge.web.controller.admin;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.beetl.sql.core.engine.PageQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import red.feng.common.utils.DateUtils;
import red.feng.knowledge.dao.BgContentsDao;
import red.feng.knowledge.model.BgContents;
import red.feng.knowledge.model.BgMetas;
import red.feng.knowledge.model.BgOptions;
import red.feng.knowledge.model.CategoryBean;
import red.feng.knowledge.service.ArticleService;
import red.feng.knowledge.service.BgOptionsService;
import red.feng.knowledge.service.MetaService;
import red.feng.system.model.SysUser;
import red.feng.system.utils.ShiroUtil;

@Controller
@RequestMapping("/admin/article")
public class ArticleCtrl {
	Logger logger=LoggerFactory.getLogger(getClass());
	@Autowired
	private BgContentsDao bgContentsDao;
	@Autowired
	private ArticleService articleService;
	@Autowired
	private BgOptionsService bgOptionsService;
	@Autowired
	private MetaService metaService;
	@RequestMapping("/list")
	public String postlist(BgContents bgArticle, @RequestParam(defaultValue = "1") int p,
			@RequestParam(defaultValue = "10") int size, ModelMap model) {
		PageQuery pageQuery = bgContentsDao.query(bgArticle, p, size);
		model.addAttribute("page", pageQuery);
		model.addAttribute("article",bgArticle);
		model.addAttribute(p);
		return "/admin/article/list";
	}
	/**
	 * 添加用户初始化页面 去增加
	 */
	@RequestMapping("/edit")
	public String add(ModelMap model) {
		SysUser user=ShiroUtil.getCurrentUser();
		BgOptions editorSize=bgOptionsService.getOption(BgOptions.KEY_EDITOR_SIZE, user.getUserId());
		if(editorSize!=null){
			model.addAttribute("editorSize", editorSize.getValue());//文章编辑器高度
		}
		//分类
		List<BgMetas> categorys=metaService.getAllCategory();
		model.addAttribute("category", categorys);
		return "/admin/article/add";
	}

	/**
	 * 保存用户 增加
	 */
	@RequestMapping(value = "/edit-post", method = RequestMethod.POST)
	public String doadd(BgContents bgArticle,@RequestParam(name="do") String status,String visibility,String date,String tags,CategoryBean category) {
		if(BgContents.STATUS_SAVE.equals(status)){
			bgArticle.setType(BgContents.TYPE_POST_DRAFT);
		}else{
			bgArticle.setType(BgContents.TYPE_POST);
		}
		bgArticle.setStatus(visibility);
		if(StringUtils.isNotBlank(date)){
			Date time=DateUtils.parse(date,DateUtils.DATE_MIDDLE_STR);
			Timestamp created=new Timestamp(time.getTime());
			bgArticle.setCreated(created);
		}else{
			bgArticle.setCreated(new Timestamp(System.currentTimeMillis()));
		}
		if (bgArticle.getCid() == null) {
			
			SysUser user=ShiroUtil.getCurrentUser();
			bgArticle.setAuthorId(user.getUserId());
			articleService.addArticle(bgArticle,category.getCategory(),tags);
		} else {
			articleService.updateArticle(bgArticle, category.getCategory(), tags);
		}
		return "redirect:/admin/article/list";
	}

	/**
	 * 
	 * 去修改
	 */
	@RequestMapping("/edit/{cid}")
	public String edit(@PathVariable Integer cid, ModelMap model) {
		SysUser user=ShiroUtil.getCurrentUser();
		BgOptions editorSize=bgOptionsService.getOption(BgOptions.KEY_EDITOR_SIZE, user.getUserId());
		if(editorSize!=null){
			model.addAttribute("editorSize", editorSize.getValue());//文章编辑器高度
		}
		//文章基本信息
		BgContents bgArticle = bgContentsDao.getById(cid);
		model.addAttribute("article", bgArticle);
		
		//tags
		String tags=metaService.getTagsByCid(cid);
		model.addAttribute("tags", tags);
		// 分类
		List<BgMetas> categorys = metaService.getAllCategory();
		model.addAttribute("category", categorys);//全部的分类
		//当前文章的分类
		String mycategory=metaService.getCategoryByCid(cid);
		model.addAttribute("mycategory", mycategory);//全部的分类	
		return "/admin/article/edit";
	}
}
