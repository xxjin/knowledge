package red.feng.knowledge.dao;

import org.beetl.sql.core.SQLManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import red.feng.knowledge.model.BgOptions;

@Repository
public class BgOptionsDao {
	@Autowired
	private SQLManager sqlManager;
	
	public void save(String name,Integer user,String value){
		BgOptions option=new BgOptions(name,user,value);
		sqlManager.insert("bgoptions.insert",option,null);
	}
	public void update(String name,Integer user,String value){
		BgOptions option=new BgOptions(name,user,value);
		 
		sqlManager.update("bgoptions.update", option);
	}
	 
	public BgOptions selectOne(String name,Integer user){
		BgOptions option=new BgOptions();
		option.setName(name);
		option.setUser(user);
		return sqlManager.selectSingle("bgoptions.selectOne", option, BgOptions.class);
	}
}
