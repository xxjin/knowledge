package red.feng.knowledge.dao;

import java.sql.Timestamp;
import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.db.KeyHolder;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import red.feng.knowledge.model.BgContents;

@Repository
public class BgContentsDao {
	@Autowired
	private SQLManager sqlManager;
	public PageQuery query(BgContents bgArticle,int page,int pagesize){
		PageQuery pageQuery=new PageQuery();
		pageQuery.setPageNumber(page);
		pageQuery.setPageSize(pagesize);
		pageQuery.setParas(bgArticle);
		pageQuery.setOrderBy("cid desc");
		sqlManager.pageQuery("bgcontents.pageQuery", BgContents.class, pageQuery);
		return pageQuery;
	}
	/**
	 * 新增文章
	 *  返回主键
	 */
	public Integer insert(BgContents bgArticle){
		bgArticle.setModified(new Timestamp(System.currentTimeMillis()));
		bgArticle.setCommentsnum(0);
		KeyHolder keyHolder=new KeyHolder();
		keyHolder.setKey("cid");
		sqlManager.insert(BgContents.class,bgArticle,keyHolder);
		return keyHolder.getInt();
	}
	 
	public void update(BgContents bgArticle){
		sqlManager.updateTemplateById(bgArticle);
	}
	
	public BgContents getById(Integer cid){
		return sqlManager.single(BgContents.class, cid);
	}
}
