package red.feng.knowledge.dao;

import java.util.List;

import org.beetl.sql.core.SQLManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import red.feng.knowledge.model.BgRelationships;

@Repository
public class BgRelationshipsDao {
	@Autowired
	private SQLManager sqlManager;
	 
	public void insert(Integer cid,Integer mid){
		BgRelationships bgRelationships = new BgRelationships();
		bgRelationships.setCid(cid);
		bgRelationships.setMid(mid);
		sqlManager.insert(bgRelationships);
	}
	
	public void delete(Integer cid){
		sqlManager.update("bgrelationship.delete", cid);
	}
}
