package red.feng.knowledge.dao;

import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import red.feng.knowledge.model.BgArticle;

@Repository
public class BgArticleDao {
	@Autowired
	private SQLManager sqlManager;
	public PageQuery query(BgArticle bgArticle,int page,int pagesize){
		PageQuery pageQuery=new PageQuery();
		pageQuery.setPageNumber(page);
		pageQuery.setPageSize(pagesize);
		pageQuery.setParas(bgArticle);
		sqlManager.pageQuery("article.pageQuery", BgArticle.class, pageQuery);
		return pageQuery;
	}
	public void add(BgArticle bgArticle){
		sqlManager.insert(bgArticle);
	}
	public BgArticle getById(Integer userId){
		return  sqlManager.selectSingle("user.findByUserId", userId, BgArticle.class);
	}
	public void update(BgArticle bgArticle){
		sqlManager.update("user.updateById", bgArticle);
	}
}
