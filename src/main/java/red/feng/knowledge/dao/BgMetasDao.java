package red.feng.knowledge.dao;

import java.util.List;

import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.db.KeyHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import red.feng.knowledge.model.BgMetas;

@Repository
public class BgMetasDao {
	@Autowired
	private SQLManager sqlManager;
	/**
	 * 保存tags
	 * @param tagstr
	 * return mid
	 */
	public Integer insert(String tagname){
		BgMetas tags=new BgMetas();
		tags.setName(tagname);
		tags.setSlug(tagname);
		tags.setCount(1);
		tags.setType(BgMetas.TYPE_TAGS);
		KeyHolder keyHolder=new KeyHolder();
		keyHolder.setKey("mid");
		sqlManager.insert(BgMetas.class,tags,keyHolder);
		return keyHolder.getInt();
	}
	public BgMetas queryTagByName(String tagname){
		BgMetas tags=new BgMetas();
		tags.setName(tagname);
		tags.setType(BgMetas.TYPE_TAGS);
		return sqlManager.selectSingle("bgmetas.sample", tags, BgMetas.class);
	}
	
	public List<BgMetas> getAllCategory(){
		BgMetas metas=new BgMetas();
		metas.setType(BgMetas.TYPE_CATEGORY);
		return sqlManager.select("bgmetas.getAllCategory", BgMetas.class, metas);
	}
	
	public void deleteTagByCid(Integer cid){
		  sqlManager.update("bgmetas.deleteTagByCid", cid);
	}
	/**
	 * 根据cid查tag
	 * @param cid
	 * @return
	 */
	public List<BgMetas> getTagsByCid(Integer cid){
		return sqlManager.select("bgmetas.getTagsByCid", BgMetas.class, cid);
	}
	/**
	 * 根据cid查tag
	 * @param cid
	 * @return
	 */
	public List<BgMetas> getCategoryByCid(Integer cid){
		return sqlManager.select("bgmetas.getCategoryByCid", BgMetas.class, cid);
	}
}
