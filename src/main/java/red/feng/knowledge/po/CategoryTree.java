package red.feng.knowledge.po;

import java.util.List;

/**
 * 分类 树
 * @author jinxx
 *
 */
public class CategoryTree {
	private Integer mid ;
	private Integer count ;
	private Integer sort ;
	private Integer parent ;
	private String description ;
	private String name ;
	private List<CategoryTree> children;
	public Integer getMid() {
		return mid;
	}
	public void setMid(Integer mid) {
		this.mid = mid;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	public Integer getParent() {
		return parent;
	}
	public void setParent(Integer parent) {
		this.parent = parent;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<CategoryTree> getChildren() {
		return children;
	}
	public void setChildren(List<CategoryTree> children) {
		this.children = children;
	}
}
