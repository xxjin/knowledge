package red.feng.knowledge.service;

import java.sql.Timestamp;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import red.feng.knowledge.dao.BgContentsDao;
import red.feng.knowledge.dao.BgRelationshipsDao;
import red.feng.knowledge.model.BgContents;

@Service
public class ArticleService {
	Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private BgContentsDao bgContentsDao;
	@Autowired
	private BgRelationshipsDao bgRelationshipsDao;
	@Autowired
	private 	MetaService metaService;
	/**
	 * 新增文章
	 * @param bgArticle
	 * @param categorys
	 * @param tags
	 */
	@Transactional
	public void addArticle(BgContents bgArticle, List<Integer> categorys, String tags) {
		Integer cid = bgContentsDao.insert(bgArticle);
		logger.debug(cid + "");
		//保存分类和tags
		metaService.saveCategory(cid, categorys);
		metaService.saveTags(cid, tags);
	}
	/**
	 * 更新文章
	 * @param bgArticle
	 * @param categorys
	 * @param tags
	 */
	@Transactional
	public void updateArticle(BgContents bgArticle, List<Integer> categorys, String tags) {
		bgArticle.setModified(new Timestamp(System.currentTimeMillis()));
		bgContentsDao.update(bgArticle);
		//保存分类和tags
		Integer cid=bgArticle.getCid();
		metaService.deleteTagByCid(cid);//删除掉metas里面的tags
		bgRelationshipsDao.delete(cid);//删除掉关系表
		metaService.saveCategory(cid, categorys);//保存
		metaService.saveTags(cid, tags);////保存
	}
}
