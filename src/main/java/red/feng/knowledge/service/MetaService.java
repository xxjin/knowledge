package red.feng.knowledge.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.beetl.sql.core.db.KeyHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import red.feng.knowledge.dao.BgContentsDao;
import red.feng.knowledge.dao.BgMetasDao;
import red.feng.knowledge.dao.BgRelationshipsDao;
import red.feng.knowledge.model.BgContents;
import red.feng.knowledge.model.BgMetas;
import red.feng.knowledge.model.BgRelationships;
import red.feng.knowledge.po.CategoryTree;

@Service
public class MetaService {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private BgRelationshipsDao bgRelationshipsDao;
	@Autowired
	private BgMetasDao bgMetasDao;

	/**
	 * 保存tag(新增)
	 * 
	 * @param cid
	 * @param tagstr
	 */
	public void saveTags(Integer cid, String tagstr) {
		
		String[] tags = tagstr.split(",");
		for (String tag : tags) {
			BgMetas meta = bgMetasDao.queryTagByName(tag);
			if (meta == null) {
				Integer mid = bgMetasDao.insert(tag);
				bgRelationshipsDao.insert(cid, mid);
			} else {
				bgRelationshipsDao.insert(cid, meta.getMid());
			}
		}
	}

	public void saveCategory(Integer cid, List<Integer> categorys) {
		if(categorys==null)return ;
		for (Integer mid : categorys) {
			if (mid != null) {
				bgRelationshipsDao.insert(cid, mid);
			}
		}
	}
	public void deleteTagByCid(Integer cid){
		bgMetasDao.deleteTagByCid(cid);
	}
	public String getTagsByCid(Integer cid) {
		List<BgMetas> tagslist = bgMetasDao.getTagsByCid(cid);
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < tagslist.size(); i++) {
			sb.append(tagslist.get(i).getName());
			if (i < tagslist.size() - 1) {
				sb.append(",");
			}
		}
		return sb.toString();
	}
	/**
	 * 获取分类，并拼成string,按逗号分隔
	 * @param cid
	 * @return
	 */
	public String getCategoryByCid(Integer cid){
		List<BgMetas> tagslist = bgMetasDao.getCategoryByCid(cid);
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < tagslist.size(); i++) {
			sb.append(tagslist.get(i).getMid());
			if (i < tagslist.size() - 1) {
				sb.append(",");
			}
		}
		return sb.toString();
	}

	/**
	 * 获取全部的分类
	 */
	public List<BgMetas> getAllCategory() {
		List<BgMetas> categorys = bgMetasDao.getAllCategory();
		List<BgMetas> tree = list2tree(categorys);
		return tree;
	}

	/**
	 * list转tree结构
	 * 
	 * @param allMetas
	 * @return
	 */
	public List<BgMetas> list2tree(List<BgMetas> allMetas) {
		List<BgMetas> tree = new ArrayList<BgMetas>();
		for (BgMetas meta : allMetas) {
			if (meta.getParent() == 0) {
				Integer level = 0;
				meta.setLevel(level);
				tree.add(meta);
				buildChildren(meta, allMetas, tree, level + 1);
			}
		}
		return tree;
	}

	private void buildChildren(BgMetas tree, List<BgMetas> allMetas, List<BgMetas> target, Integer level) {
		Integer id = tree.getMid();
		for (BgMetas child : allMetas) {
			if (id.equals(child.getParent())) {

				child.setLevel(level);
				target.add(child);
				buildChildren(child, allMetas, target, level);
			}
		}
	}

	public static void main(String[] args) {
		MetaService service = new MetaService();
		List<BgMetas> tree = new ArrayList<BgMetas>();
		BgMetas meta = new BgMetas();
		meta.setMid(1);
		meta.setParent(0);
		meta.setName("aaa");
		tree.add(meta);
		BgMetas meta2 = new BgMetas();
		meta2.setMid(2);
		meta2.setParent(1);
		meta2.setName("bbb");
		tree.add(meta2);
		BgMetas meta4 = new BgMetas();
		meta4.setMid(3);
		meta4.setParent(0);
		meta4.setName("ccc");
		tree.add(meta4);
		List<BgMetas> target = service.list2tree(tree);
		for (BgMetas meta3 : target) {
			for (int i = 0; i < meta3.getLevel(); i++) {
				System.out.print("*");
			}
			System.out.println(meta3.getLevel() + "=" + meta3.getMid() + "=" + meta3.getName());
		}
	}
}
