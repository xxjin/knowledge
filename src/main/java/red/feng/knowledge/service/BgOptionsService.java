package red.feng.knowledge.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import red.feng.knowledge.dao.BgOptionsDao;
import red.feng.knowledge.model.BgOptions;

@Service
public class BgOptionsService {

	@Autowired
	private BgOptionsDao bgOptionsDao;

	/**
	 * 更新配置项（没有则增加）
	 * @param name
	 * @param user
	 * @param value
	 */
	public void updateOption(String name, Integer user, String value) {
		BgOptions option = bgOptionsDao.selectOne(name, user);
		if (option == null) {
			bgOptionsDao.save(name, user, value);
		} else {
			bgOptionsDao.update(name, user, value);
		}
	}
	
	/**
	 * 查询选项，用户级查询不到时，查询系统级
	 * @param name
	 * @param user
	 * @return
	 */
	public BgOptions getOption(String name, Integer user){
		if(user==BgOptions.DEFAULT_USER){
			return bgOptionsDao.selectOne(name, user);
		}
		else{
			BgOptions option=bgOptionsDao.selectOne(name, user);
			if(option==null){
				option=bgOptionsDao.selectOne(name, BgOptions.DEFAULT_USER);
			}
			return option;
		}
	}
	
}
