package red.feng.knowledge.model;
import java.math.*;
import java.util.Date;

import org.beetl.sql.core.annotatoin.Table;

import java.sql.Timestamp;

/*
* 
* gen by beetlsql 2016-08-13
*/
@Table(name="blog.bg_relationships")
public class BgRelationships  {
	private Integer cid ;
	private Integer mid ;
	public Integer getCid() {
		return cid;
	}
	public void setCid(Integer cid) {
		this.cid = cid;
	}
	public Integer getMid() {
		return mid;
	}
	public void setMid(Integer mid) {
		this.mid = mid;
	}

}
