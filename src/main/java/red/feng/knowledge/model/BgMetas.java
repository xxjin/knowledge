package red.feng.knowledge.model;
import java.math.*;
import java.util.Date;
import java.util.List;

import org.beetl.sql.core.annotatoin.Table;

import java.sql.Timestamp;

/*
* 
* gen by beetlsql 2016-08-13
*/
@Table(name="blog.bg_metas")
public class BgMetas  {
	public static final String TYPE_CATEGORY="category";
	public static final String TYPE_TAGS="tag";
	private Integer mid ;
	private Integer count ;
	private Integer sort ;
	private Integer parent ;
	private String description ;
	private String name ;
	private String slug ;
	private String type ;
	
	
	//非数据库字段
	private Integer level;//层级    
	public Integer getMid() {
		return mid;
	}
	public void setMid(Integer mid) {
		this.mid = mid;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	public Integer getParent() {
		return parent;
	}
	public void setParent(Integer parent) {
		this.parent = parent;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSlug() {
		return slug;
	}
	public void setSlug(String slug) {
		this.slug = slug;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getLevel() {
		return level;
	}
	public void setLevel(Integer level) {
		this.level = level;
	}
 

}
