package red.feng.knowledge.model;

import java.util.List;
/**
 * 接受前台穿的list
 * @author jinxx
 *
 */
public class CategoryBean {
	private List<Integer> category;

	public List<Integer> getCategory() {
		return category;
	}

	public void setCategory(List<Integer> category) {
		this.category = category;
	}
}
