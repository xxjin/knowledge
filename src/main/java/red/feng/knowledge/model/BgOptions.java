package red.feng.knowledge.model;
import java.math.*;
import java.util.Date;

import org.beetl.sql.core.annotatoin.Table;

import java.sql.Timestamp;

/*
* 
* gen by beetlsql 2016-08-15
*/
@Table(name="blog.bg_options")
public class BgOptions  {
	public static final String KEY_EDITOR_SIZE="editorSize"; //编辑器size高度大小
	public static final Integer DEFAULT_USER=0;
	private String name ;//key
	private Integer user ;//用户，0表示默认
	private String value ;//value
	public BgOptions() {
	}
	public BgOptions(String name, Integer user, String value) {
		this.name = name;
		this.user = user;
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getUser() {
		return user;
	}
	public void setUser(Integer user) {
		this.user = user;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

}
