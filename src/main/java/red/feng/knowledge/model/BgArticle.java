package red.feng.knowledge.model;

import java.math.*;
import java.util.Date;

import org.beetl.sql.core.annotatoin.Table;

import red.feng.system.model.BaseEntity;

import java.sql.Timestamp;

/*
* 
* gen by beetlsql 2016-08-06
*/
@Table(name = "blog.bg_article")
public class BgArticle extends BaseEntity {
	private String title; // 标题
	private String content; // 内容
	private String excerpt; // 摘要
	private Integer type; // 文章类型 1 原创、2转载、3翻译
	private Integer author;//作者
	private Integer commentStatus; // 评论状态
	private Integer status; // 文章状态，草稿，已发布
	private Integer systemType; // 系统分类
	private Integer category; // 个人分类

	public Integer getSystemType() {
		return systemType;
	}

	public void setSystemType(Integer systemType) {
		this.systemType = systemType;
	}

	 

	public Integer getAuthor() {
		return author;
	}

	public void setAuthor(Integer author) {
		this.author = author;
	}

	public Integer getCommentStatus() {
		return commentStatus;
	}

	public void setCommentStatus(Integer commentStatus) {
		this.commentStatus = commentStatus;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getExcerpt() {
		return excerpt;
	}

	public void setExcerpt(String excerpt) {
		this.excerpt = excerpt;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getCategory() {
		return category;
	}

	public void setCategory(Integer category) {
		this.category = category;
	}

}
