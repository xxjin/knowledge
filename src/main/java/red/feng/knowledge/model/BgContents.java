package red.feng.knowledge.model;
import java.math.*;
import java.util.Date;

import org.beetl.sql.core.annotatoin.Table;

import java.sql.Timestamp;

/*
* 
* gen by beetlsql 2016-08-12
*/
@Table(name="blog.bg_contents")
public class BgContents  {
	public static final String TYPE_POST="post";//文章
	public static final String TYPE_POST_DRAFT="post_draft";//文章草稿
	public static final String TYPE_PAGE="page";//页面
	public static final String TYPE_PAGE_DRAFT="page_draft";//页面草稿
	public static final String STATUS_SAVE="save";//保存
	public static final String STATUS_PUBLISH="publish";//发布
	public static final String STATUS_HIDDEN="hidden";//隐藏
	public static final String STATUS_PASSWORD="password";//密码保护
	public static final String STATUS_PRIVATE="private";//私密
	public static final String STATUS_WAITING="waiting";//待审核
	private Integer cid ;
	private Integer authorId ;
	private Integer commentsnum ;//评论数，冗余
	private Timestamp created ;
	private Timestamp modified ;//修改时间
	private Integer sort ;//排序
	private Integer parent ;
	private String allowcomment ;//允许评论
	private String allowfeed ;//允许在聚合中出现
	private String allowping ;//允许被引用
	private String password ;
	private String slug ;
	private String status ;
	private String template ;
	private String text ;
	private String title ;//标题
	private String type ;//类型，页面还是文章
	//ext collumn
	private String authorName;
	public Integer getCid() {
		return cid;
	}
	public void setCid(Integer cid) {
		this.cid = cid;
	}
	public Integer getAuthorId() {
		return authorId;
	}
	public void setAuthorId(Integer authorId) {
		this.authorId = authorId;
	}
	public Integer getCommentsnum() {
		return commentsnum;
	}
	public void setCommentsnum(Integer commentsnum) {
		this.commentsnum = commentsnum;
	}
	 
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	public Integer getParent() {
		return parent;
	}
	public void setParent(Integer parent) {
		this.parent = parent;
	}
	public String getAllowcomment() {
		return allowcomment;
	}
	public void setAllowcomment(String allowcomment) {
		this.allowcomment = allowcomment;
	}
	public String getAllowfeed() {
		return allowfeed;
	}
	public void setAllowfeed(String allowfeed) {
		this.allowfeed = allowfeed;
	}
	public String getAllowping() {
		return allowping;
	}
	public void setAllowping(String allowping) {
		this.allowping = allowping;
	}
	public Timestamp getCreated() {
		return created;
	}
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	public Timestamp getModified() {
		return modified;
	}
	public void setModified(Timestamp modified) {
		this.modified = modified;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSlug() {
		return slug;
	}
	public void setSlug(String slug) {
		this.slug = slug;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTemplate() {
		return template;
	}
	public void setTemplate(String template) {
		this.template = template;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAuthorName() {
		return authorName;
	}
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	
}
